<?php

declare(strict_types=1);

namespace SimpleSAML\Module\privacyidea\Auth;

use PrivacyIDEA\PHPClient\PIResponse;
use PrivacyIDEA\PHPClient\PrivacyIDEA;
use SimpleSAML\Auth\ProcessingChain;
use SimpleSAML\Auth\State;
use SimpleSAML\Logger;
use SimpleSAML\Module\privacyidea\Auth\Source\PrivacyideaAuthSource;
use SimpleSAML\Session;

class Utils
{
    /**
     * Perform 2FA given the current state and the inputs from the form.
     *
     * @param array $state      state
     * @param array $formParams inputs from the form
     *
     * @return PIResponse|null an array containing attributes and detail, or NULL
     */
    public static function authenticatePI(array &$state, array $formParams)
    {
        assert(gettype($state) === 'array');
        assert(gettype($formParams) === 'array');

        Logger::debug("privacyIDEA: Utils::authenticatePI with form data:\n" . http_build_query($formParams, '', ', '));

        $state['privacyidea:privacyidea:ui']['mode'] = $formParams['mode'];

        // If the mode was changed, do not make any requests
        if ($formParams['modeChanged'] === true) {
            $state['privacyidea:privacyidea:ui']['loadCounter'] = 1;

            return null;
        }

        $serverConfig = $state['privacyidea:privacyidea'];

        // Get the username from elsewhere if it is not in the form
        if (empty($formParams['username'])) {
            if ($state['privacyidea:privacyidea']['authenticationMethod'] === 'authsource') {
                $username = $state['privacyidea:privacyidea']['username'];
            } else {
                $username = $state['Attributes'][$serverConfig['uidKey']][0];
            }
        } else {
            $username = $formParams['username'];
        }

        $pi = self::createPrivacyIDEAInstance($serverConfig);
        if ($pi === null) {
            throw new \Exception('Unable to initialize privacyIDEA');
        }

        $response = null;
        $transactionID = '';
        if (isset($state['privacyidea:privacyidea']['transactionID'])) {
            $transactionID = $state['privacyidea:privacyidea']['transactionID'];
        }

        // Send a request according to the mode
        if ($formParams['mode'] === 'push') {
            try {
                if ($pi->pollTransaction($transactionID)) {
                    // If the authentication has been confirmed on the phone,
                    // the authentication has to be finalized with a
                    // call to /validate/check with an empty pass
                    // https://privacyidea.readthedocs.io/en/latest/tokens/authentication_modes.html#outofband-mode
                    $response = $pi->validateCheck($username, '', $transactionID);
                }
            } catch (\Exception $e) {
                self::handlePrivacyIDEAException($e, $state);
            }
        } elseif ($formParams['mode'] === 'u2f') {
            $u2fSignResponse = $formParams['u2fSignResponse'];

            if (empty($u2fSignResponse)) {
                Logger::error('privacyIDEA: Incomplete data for U2F authentication: u2fSignResponse is missing!');
            } else {
                try {
                    $response = $pi->validateCheckU2F($username, $transactionID, $u2fSignResponse);
                } catch (\Exception $e) {
                    self::handlePrivacyIDEAException($e, $state);
                }
            }
        } elseif ($formParams['mode'] === 'webauthn') {
            $origin = $formParams['origin'];
            $webAuthnSignResponse = $formParams['webAuthnSignResponse'];

            if (empty($origin) || empty($webAuthnSignResponse)) {
                Logger::error(
                    'privacyIDEA: Incomplete data for WebAuthn authentication: '
                    . 'WebAuthnSignResponse or Origin is missing!'
                );
            } else {
                try {
                    $response = $pi->validateCheckWebAuthn($username, $transactionID, $webAuthnSignResponse, $origin);
                } catch (\Exception $e) {
                    self::handlePrivacyIDEAException($e, $state);
                }
            }
        } elseif ($formParams['mode'] === 'totp') {
            try {
                // limit otp validation to totp tokens to prevent incrementing of webauthn failcounter
                $params["type"] = "totp";
                $params["user"] = $username;
                $params["pass"] = $formParams['otp'];
                $headers = [];

                $rawResponse = $pi->sendRequest($params, $headers, 'POST', '/validate/check');
                $response = PIResponse::fromJSON($rawResponse, $pi);

                $isAuthUnuccessful = $response->value === false;
                if ($isAuthUnuccessful) {
                    // prepare custom error message placeholder - failcounter might have been exceeded
                    Logger::debug("Original TOTP validation response error message: " . $response->errorMessage);
                    $response->errorMessage = "possible failcounter exceeded";
                }
            } catch (\Exception $e) {
                self::handlePrivacyIDEAException($e, $state);
            }
        } else {
            // Backup code validation
            try {
                $response = $pi->validateCheck($username, $formParams['otp'], $transactionID);
            } catch (\Exception $e) {
                self::handlePrivacyIDEAException($e, $state);
            }
        }
        $counter = $formParams['loadCounter'];
        $state['privacyidea:privacyidea:ui']['loadCounter'] = $counter + 1;

        return $response;
    }

    public static function handlePrivacyIDEAException($exception, &$state)
    {
        Logger::error('Exception: ' . $exception->getMessage());
        $state['privacyidea:privacyidea']['errorCode'] = $exception->getCode();
        $state['privacyidea:privacyidea']['errorMessage'] = $exception->getMessage();
    }

    /**
     * Write SSO specific data to the session and register a logout handler. The logout handler has to be attached to an
     * authority which is also obtained from the session. If there are no valid authorities, this function does nothing.
     * The first authority returned by SSP is used. Authorities are validated before they are returned so the authority
     * that is used can be considered valid.
     */
    public static function tryWriteSSO()
    {
        Logger::debug('privacyIDEA: tryWriteSSO');

        $session = Session::getSessionFromRequest();
        // First get the authority to register the logout handler for
        $authorities = $session->getAuthorities();
        if (empty($authorities)) {
            Logger::error(
                'privacyIDEA: Cannot use SSO because there is no authority'
                . 'configured to register the logout handler for!'
            );

            return;
        }

        $authority = $authorities[0];
        Logger::debug('privacyIDEA: Registering logout handler for authority ' . $authority);

        $session->registerLogoutHandler($authority, self::class, 'handleLogout');
        $session->setData('privacyidea:privacyidea', '2FA-success', true, Session::DATA_TIMEOUT_SESSION_END);
        Logger::info('privacyIDEA: saved 2FA-success');
        Logger::debug('privacyIDEA: SSO data written and logout handler registered.');
    }

    /**
     * Check the state for data indicating an active login. If such data is present, check if SSO data of our module is
     * present, indicating that 2FA was completed before. A boolean is returned to indicate if the login/2FA can be
     * skipped.
     *
     * @param $state
     *
     * @return bool true if login/2FA can be skipped, false if not
     */
    public static function checkForValidSSO($state)
    {
        Logger::debug('privacyIDEA: checkForValidSSO');

        // For SSO to be valid, we check 2 things:
        // 1. Valid login of SSP which is not expired
        // 2. Completed 2FA with this module
        if (is_array($state) && array_key_exists('Expire', $state) && $state['Expire'] > time()) {
            Logger::debug('privacyIDEA: Valid login found. Checking for valid 2FA..');
            $session = Session::getSessionFromRequest();

            return $session->getData('privacyidea:privacyidea', '2FA-success');
        }

        Logger::debug('privacyIDEA: No valid login found or state is not an array.');

        return false;
    }

    /**
     * This function is registered as a logout handler when writing the SSO specific data to the session. When called,
     * it removes SSO data on logout.
     */
    public static function handleLogout()
    {
        Logger::debug('privacyIDEA: Logout handler called. Removing SSO data.');
        Session::getSessionFromRequest()->deleteData('privacyidea:privacyidea', '2FA-success');
    }

    /**
     * Create a new privacyIDEA object with the given configuration.
     *
     * @param array $config
     *
     * @return PrivacyIDEA|null privacyIDEA object or null on error
     */
    public static function createPrivacyIDEAInstance($config)
    {
        if (!empty($config['privacyideaServerURL'])) {
            $pi = new PrivacyIDEA('simpleSAMLphp', $config['privacyideaServerURL']);
            $pi->logger = new PILogger();

            if (array_key_exists('timeout', $config) && is_int($config['timeout'])) {
                $pi->timeout = $config['timeout'];
            }

            if (array_key_exists('connectTimeout', $config) && is_int($config['connectTimeout'])) {
                $pi->connectTimeout = $config['connectTimeout'];
            }

            if (array_key_exists('sslVerifyHost', $config) && !empty($config['sslVerifyHost'])) {
                $pi->sslVerifyHost = $config['sslVerifyHost'] !== false;
            }

            if (array_key_exists('sslVerifyPeer', $config) && !empty($config['sslVerifyPeer'])) {
                $pi->sslVerifyPeer = $config['sslVerifyPeer'] !== false;
            }

            if (array_key_exists('serviceAccount', $config) && !empty($config['serviceAccount'])) {
                $pi->serviceAccountName = $config['serviceAccount'];
            }

            if (array_key_exists('servicePass', $config) && !empty($config['servicePass'])) {
                $pi->serviceAccountPass = $config['servicePass'];
            }

            if (array_key_exists('serviceRealm', $config) && !empty($config['serviceRealm'])) {
                $pi->serviceAccountRealm = $config['serviceRealm'];
            }

            if (array_key_exists('realm', $config) && !empty($config['realm'])) {
                $pi->realm = $config['realm'];
            }

            return $pi;
        }

        Logger::error('privacyIDEA: Cannot create privacyIDEA instance: server url missing in configuration!');

        return null;
    }

    /**
     * Process the response from privacyIDEA and write information for the next step to the state. If the response from
     * privacyIDEA indicates success and this module is used as AuthProcFilter, this function will resume the processing
     * chain and not return.
     *
     * @param string $stateId  to load the state
     * @param mixed  $response from privacyIDEA
     *
     * @return string stateId of the modified state
     */
    public static function processPIResponse($stateId, PIResponse $response)
    {
        assert(gettype($stateId) === 'string');
        $state = State::loadState($stateId, 'privacyidea:privacyidea');

        $config = $state['privacyidea:privacyidea'];
        $state['privacyidea:privacyidea:ui']['mode'] = 'otp';

        if (!empty($response->multiChallenge)) {
            // Authentication not complete, new challenges where triggered. Prepare the state for the next step
            $triggeredToken = $response->triggeredTokenTypes();
            // Preferred token type
            if ($config !== null && array_key_exists('preferredTokenType', $config)) {
                $preferred = $config['preferredTokenType'];
                if (!empty($preferred)) {
                    if (in_array($preferred, $triggeredToken, true)) {
                        $state['privacyidea:privacyidea:ui']['mode'] = $preferred;
                    }
                }
            }

            $state['privacyidea:privacyidea:ui']['pushAvailable'] = in_array('push', $triggeredToken, true);
            $state['privacyidea:privacyidea:ui']['otpAvailable'] = true; // Always show otp field
            $state['privacyidea:privacyidea:ui']['message'] = $response->messages;

            if (in_array('webauthn', $triggeredToken, true)) {
                $state['privacyidea:privacyidea:ui']['webAuthnSignRequest'] = $response->webAuthnSignRequest();
            }

            if (in_array('u2f', $triggeredToken, true)) {
                $state['privacyidea:privacyidea:ui']['u2fSignRequest'] = $response->u2fSignRequest();
            }

            $state['privacyidea:privacyidea']['transactionID'] = $response->transactionID;
        } elseif ($response->value) {
            // Authentication successful. Finalize the authentication depending on method (AuthProc or AuthSource) and
            // write SSO specific data if enabled.
            Logger::debug('privacyIDEA: User authenticated successfully!');

            // Complete the authentication depending on method
            if ($state['privacyidea:privacyidea']['authenticationMethod'] === 'authprocess') {
                // Write data for SSO if enabled
                if (array_key_exists('SSO', $config) && $config['SSO'] === true) {
                    self::tryWriteSSO();
                }

                State::saveState($state, 'privacyidea:privacyidea');
                ProcessingChain::resumeProcessing($state);
            } elseif ($state['privacyidea:privacyidea']['authenticationMethod'] === 'authsource') {
                // For AuthSource, the attributes required by saml need to be present,
                // so check for that before completing
                PrivacyideaAuthSource::checkAuthenticationComplete($state, $response, $config);
            }
        } elseif (!empty($response->errorCode)) {
            // privacyIDEA returned an error, prepare to display it
            Logger::error(
                'privacyIDEA: Error code: ' . $response->errorCode . ', Error message: ' . $response->errorMessage
            );
            $state['privacyidea:privacyidea']['errorCode'] = $response->errorCode;
            $state['privacyidea:privacyidea']['errorMessage'] = $response->errorMessage;
        } else {
            // Unexpected response
            Logger::error('privacyIDEA: ' . $response->message);
            $state['privacyidea:privacyidea']['errorMessage'] = $response->errorMessage;
        }

        return State::saveState($state, 'privacyidea:privacyidea');
    }

    /**
     * Determine the clients IP-Address.
     *
     * @return string|null the IP-Address of the client
     */
    public static function getClientIP()
    {
        $result = ($_SERVER['HTTP_X_FORWARDED_FOR'] ?? null) ?: ($_SERVER['REMOTE_ADDR'] ?? null)
            ?: ($_SERVER['HTTP_CLIENT_IP'] ?? null);
        Logger::debug('privacyIDEA: client ip: ' . $result);

        return $result;
    }

    /**
     * Find the first usable uid key. If the administrator has configured multiple uidKeys, this will find the first one
     * that exists as an Attribute in the $state and update the $config to use that key.
     *
     * @param array $config The authproc configuration to use
     * @param array $state  The global state to check the keys against
     *
     * @return array The updated config
     */
    public static function checkUidKey(array $config, array $state)
    {
        assert(gettype($config) === 'array');
        assert(gettype($state) === 'array');

        if (gettype($config['uidKey']) === 'array' && !empty($config['uidKey'])) {
            foreach ($config['uidKey'] as $i) {
                if (isset($state['Attributes'][$i][0])) {
                    $config['uidKey'] = $i;
                }
            }
        }

        return $config;
    }
}
