# privacyIDEA simpleSAMLphp Plugin

This plugin adds flexible, enterprise-grade two factor authentication to simplesSAMLphp.

It enables simpleSAMLphp to do two factor authentication against
a [privacyIDEA server](https://github.com/privacyidea/privacyidea),
that runs in your network. Users can authenticate with normal OTP tokens,
Challenge Response tokens like e-mail and SMS or using WebAuthn and U2F devices.
TiQR is currently not supported.

## Installation

It is recommended to install this package using [composer](https://getcomposer.org/). In your saml root dir, execute the following command in a terminal:

`composer require cesnet/simplesamlphp-module-privacyidea`

## Configuration

Please check the [documentation](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/-/blob/main/docs/privacyidea.md)

## Disclaimer

This is a fork of [privacyidea/simplesamlphp-module-privacyidea](https://github.com/privacyidea/simplesamlphp-module-privacyidea) by [@privacyidea](https://github.com/privacyidea), maintained and contributed to by CESNET, z. s. p. o. and Institute of Computer Science, Masaryk University.
