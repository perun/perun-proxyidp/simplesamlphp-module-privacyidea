<?php

declare(strict_types=1);

use SimpleSAML\Auth\Source;
use SimpleSAML\Auth\State;
use SimpleSAML\Logger;
use SimpleSAML\Module;
use SimpleSAML\Module\core\Auth\UserPassBase;
use SimpleSAML\Module\privacyidea\Auth\Source\PrivacyideaAuthSource;
use SimpleSAML\Module\privacyidea\Auth\Utils;
use SimpleSAML\Session;
use SimpleSAML\Utils\HTTP;

$stateId = Session::getSessionFromRequest()->getData('privacyidea:privacyidea', 'stateId');
Session::getSessionFromRequest()->deleteData('privacyidea:privacyidea', 'stateId');
if (empty($stateId)) {
    Logger::error('stateId empty in FormReceiver.');
    throw new \Exception('State information lost!');
}
$state = State::loadState($stateId, 'privacyidea:privacyidea');

// Find the username
if (array_key_exists('username', $_REQUEST)) {
    $username = (string) $_REQUEST['username'];
} elseif (isset($state['privacyidea:privacyidea']['uidKey'])) {
    $uidKey = $state['privacyidea:privacyidea']['uidKey'];
    $username = $state['Attributes'][$uidKey][0];
} elseif (isset($state['privacyidea:privacyidea']['username'])) {
    $username = $state['privacyidea:privacyidea']['username'];
} elseif (isset($state['core:username'])) {
    $username = (string) $state['core:username'];
} else {
    $username = '';
}

$formParams = [
    'username' => $username,
    'pass' => array_key_exists('password', $_REQUEST) ? $_REQUEST['password'] : '',
    'otp' => array_key_exists('otp', $_REQUEST) ? $_REQUEST['otp'] : '',
    'mode' => array_key_exists('mode', $_REQUEST) ? $_REQUEST['mode'] : 'otp',
    'pushAvailable' => ($_REQUEST['pushAvailable'] ?? 'false') === 'true',
    'otpAvailable' => ($_REQUEST['otpAvailable'] ?? 'true') === 'true',
    'modeChanged' => ($_REQUEST['modeChanged'] ?? 'false') === 'true',
    'webAuthnSignResponse' => array_key_exists(
        'webAuthnSignResponse',
        $_REQUEST
    ) ? $_REQUEST['webAuthnSignResponse'] : '',
    'webAuthnSignRequest' => array_key_exists('webAuthnSignRequest', $_REQUEST) ? $_REQUEST['webAuthnSignRequest'] : '',
    'origin' => array_key_exists('origin', $_REQUEST) ? $_REQUEST['origin'] : '',
    'u2fSignRequest' => array_key_exists('u2fSignRequest', $_REQUEST) ? $_REQUEST['u2fSignRequest'] : '',
    'u2fSignResponse' => array_key_exists('u2fSignResponse', $_REQUEST) ? $_REQUEST['u2fSignResponse'] : '',
    'message' => array_key_exists('message', $_REQUEST) ? $_REQUEST['message'] : '',
    'loadCounter' => array_key_exists('loadCounter', $_REQUEST) ? $_REQUEST['loadCounter'] : 1,
];

if ($state['privacyidea:privacyidea']['authenticationMethod'] === 'authprocess') {
    // Auth Proc
    try {
        $response = Utils::authenticatePI($state, $formParams);
        $stateId = State::saveState($state, 'privacyidea:privacyidea');

        // If the authentication is successful processPIResponse will not return!
        if (!empty($response)) {
            $stateId = Utils::processPIResponse($stateId, $response);
        }
        $url = Module::getModuleURL('privacyidea/FormBuilder.php');
        HTTP::redirectTrustedURL(
            $url,
            [
            'stateId' => $stateId,
            ]
        );
    } catch (Exception $e) {
        Logger::error($e->getMessage());
    }
} else {
    // Auth Source
    $source = Source::getById($state['privacyidea:privacyidea']['AuthId']);
    if ($source->getRememberUsernameEnabled()) {
        $sessionHandler = SessionHandler::getSessionHandler();
        $params = $sessionHandler->getCookieParams();

        $params['expire'] = time();
        $params['expire'] += (isset($_REQUEST['rememberUsername']) && $_REQUEST['rememberUsername'] === 'Yes'
            ? 31536000 : -300);
        HTTP::setCookie($source->getAuthId() . '-username', $username, $params, false);
    }

    if ($source->isRememberMeEnabled()) {
        if (array_key_exists('rememberMe', $_REQUEST) && $_REQUEST['rememberMe'] === 'Yes') {
            $state['RememberMe'] = true;
            $stateId = State::saveState($state, UserPassBase::STAGEID);
        }
    }

    try {
        PrivacyideaAuthSource::authSourceLogin($stateId, $formParams);
    } catch (Exception $e) {
        Logger::error($e->getMessage());
        $state = State::loadState($stateId, 'privacyidea:privacyidea');
        $state['privacyidea:privacyidea']['errorCode'] = $e->getCode();
        $state['privacyidea:privacyidea']['errorMessage'] = $e->getMessage();
        $stateId = State::saveState($state, 'privacyidea:privacyidea');
    }
}
