<?php

declare(strict_types=1);

use SimpleSAML\Auth\Source;
use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Logger;
use SimpleSAML\Module;
use SimpleSAML\Session;
use SimpleSAML\XHTML\Template;

Logger::debug('Loading privacyIDEA form..');
// Load $state from the earlier position
$stateId = $_REQUEST['stateId'];
if (empty($stateId)) {
    Logger::error('Unable to load state information because stateId is lost');
    throw new \Exception('State information lost!');
}
$state = State::loadState($stateId, 'privacyidea:privacyidea');

// Find the username
if (isset($state['privacyidea:privacyidea']['uidKey'])) {
    $uidKey = $state['privacyidea:privacyidea']['uidKey'];
    $username = $state['Attributes'][$uidKey][0];
} elseif (isset($state['privacyidea:privacyidea']['username'])) {
    $username = $state['privacyidea:privacyidea']['username'];
} elseif (isset($state['core:username'])) {
    $username = (string) $state['core:username'];
} else {
    $username = '';
}

// Prepare the form to show
$tpl = new Template(Configuration::getInstance(), 'privacyidea:LoginForm.php');

// Prepare error to show in UI
$tpl->data['errorCode'] = null;
$tpl->data['errorMessage'] = null;

if (
    !empty($state['privacyidea:privacyidea']['errorCode'])
    || !empty($state['privacyidea:privacyidea']['errorMessage'])
) {
    $tpl->data['errorCode'] = ($state['privacyidea:privacyidea']['errorCode'] ?? null) ?: '';
    $state['privacyidea:privacyidea']['errorCode'] = '';
    $tpl->data['errorMessage'] = $tpl->t('{privacyidea:privacyidea:error_message}');

    // replace custom error message placeholder
    $errorMessage = $state['privacyidea:privacyidea']['errorMessage'];
    if (stripos($errorMessage, "possible failcounter exceeded") !== false) {
        $tpl->data['errorMessage'] = $tpl->t('{privacyidea:privacyidea:failcounter_error_message}');
    }

    $state['privacyidea:privacyidea']['errorMessage'] = '';
    $stateId = State::saveState($state, 'privacyidea:privacyidea');
}

// AuthProc
if ($state['privacyidea:privacyidea']['authenticationMethod'] === 'authprocess') {
    $tpl->data['authProcFilterScenario'] = true;
    $tpl->data['rememberUsernameEnabled'] = true;
    $tpl->data['rememberUsernameChecked'] = true;
    $tpl->data['forceUsername'] = true;

    // Enroll token's QR
    if (isset($state['privacyidea:tokenEnrollment']['tokenQR'])) {
        $tpl->data['tokenQR'] = $state['privacyidea:tokenEnrollment']['tokenQR'];
    } else {
        $tpl->data['tokenQR'] = null;
    }
} elseif ($state['privacyidea:privacyidea']['authenticationMethod'] === 'authsource') {
    // AuthSource
    $source = Source::getById($state['privacyidea:privacyidea']['AuthId']);

    if ($source === null) {
        Logger::error('Could not find authentication source with ID ' . $state['privacyidea:privacyidea']['AuthId']);
    }

    $tpl->data['username'] = $username;
    $tpl->data['rememberMeEnabled'] = $source->isRememberMeEnabled();
    $tpl->data['rememberMeChecked'] = $source->isRememberMeChecked();
    $tpl->data['links'] = $source->getLoginLinks();

    if (array_key_exists('forcedUsername', $state)) {
        $tpl->data['forceUsername'] = true;
        $tpl->data['rememberUsernameEnabled'] = false;
        $tpl->data['rememberUsernameChecked'] = false;
    } else {
        $tpl->data['forceUsername'] = false;
        $tpl->data['rememberUsernameEnabled'] = $source->getRememberUsernameEnabled();
        $tpl->data['rememberUsernameChecked'] = $source->getRememberUsernameChecked();
    }

    $tpl->data['SPMetadata'] = @$state['SPMetadata'];
}

// Get all the ui data placed in state and set it to $tpl->data for future use in LoginForm.php
if (!empty($state['privacyidea:privacyidea:ui'])) {
    foreach ($state['privacyidea:privacyidea:ui'] as $key => $value) {
        $tpl->data[$key] = $value;
    }
}

// Make sure every required key exists, even with just default values
if (!array_key_exists('head', $tpl->data)) {
    $tpl->data['head'] = '';
}

if (empty($_REQUEST['loadCounter'])) {
    $tpl->data['loadCounter'] = 1;
}

if ($state['privacyidea:privacyidea']['authenticationMethod'] === 'authprocess') {
    $tpl->data['LogoutURL'] = Module::getModuleURL(
        'core/authenticate.php',
        [
        'as' => $state['Source']['auth'],
        ]
    ) . '&logout';
}

$translator = $tpl->getTranslator();

$tpl->data['otpAvailable'] = true; // FIXME
$tpl->data['u2fAvailable'] = !empty($tpl->data['u2fSignRequest']);
$tpl->data['webauthnAvailable'] = !empty($tpl->data['webAuthnSignRequest']);
$tpl->data['pushAvailable'] = false; // FIXME
$tpl->data['mode'] = ($tpl->data['mode'] ?? null) ?: 'otp';
$tpl->data['noAlternatives'] = true;
foreach (['otp', 'push', 'u2f', 'webauthn'] as $mode) {
    if ($mode !== $tpl->data['mode'] && $tpl->data[$mode . 'Available']) {
        $tpl->data['noAlternatives'] = false;
        break;
    }
}

// Set default scenario if isn't set
if (!empty($tpl->data['authProcFilterScenario'])) {
    if (empty($tpl->data['username'])) {
        $tpl->data['username'] = '';
    }
} else {
    $tpl->data['authProcFilterScenario'] = 0;
}

// Set the right text shown in otp/pass field(s)
if (isset($tpl->data['otpFieldHint'])) {
    $tpl->data['otpHint'] = $tpl->data['otpFieldHint'];
} else {
    $tpl->data['otpHint'] = $translator->t('{privacyidea:privacyidea:otp}');
}
if (isset($tpl->data['passFieldHint'])) {
    $tpl->data['passHint'] = $tpl->data['passFieldHint'];
} else {
    $tpl->data['passHint'] = $translator->t('{privacyidea:privacyidea:password}');
}

// Prepare next settings
if (!empty($tpl->data['username'])) {
    $tpl->data['autofocus'] = 'password';
} else {
    $tpl->data['autofocus'] = 'username';
}

$messageOverride = $tpl->data['messageOverride'] ?? null;
if ($messageOverride === null || is_string($messageOverride)) {
    $tpl->data['messageOverride'] = $messageOverride ?? $tpl->data['message'] ?? '';
} elseif (is_callable($messageOverride)) {
    $tpl->data['messageOverride'] = call_user_func($messageOverride, $tpl->data['message'] ?? '');
}

$translations = [];
$translation_keys = [
    'webauthn_insecure_context',
    'webauthn_library_unavailable',
    'webauthn_AbortError',
    'webauthn_InvalidStateError',
    'webauthn_NotAllowedError',
    'webauthn_NotSupportedError',
    'webauthn_TypeError',
    'webauthn_other_error',
    'webauthn_in_progress',
    'webauthn_success',
    'u2f_insecure_context',
    'u2f_unavailable',
    'u2f_sign_request_error',
    'try_again',
];
foreach ($translation_keys as $translation_key) {
    $translations[$translation_key] = $tpl->t(sprintf('{privacyidea:privacyidea:%s}', $translation_key));
}
$tpl->data['translations'] = $translations;

Session::getSessionFromRequest()->setData('privacyidea:privacyidea', 'stateId', $stateId);
$tpl->show();
