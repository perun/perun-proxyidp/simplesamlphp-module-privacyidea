// Helper functions
function getElement(id) {
  return document.getElementById(id);
}

function value(id) {
  const element = getElement(id);
  return element === null ? "" : element.value;
}

function getContent(id) {
  const element = getElement(id);
  return element === null ? "" : element.content;
}

function booleanValue(id) {
  return value(id) === "true";
}

function set(id, value) {
  const element = getElement(id);
  if (element != null) {
    element.value = value;
  }
}

function hide(id) {
  const element = getElement(id);
  if (element != null) {
    element.classList.add("hidden");
  }
}

function show(id) {
  const element = getElement(id);
  if (element != null) {
    element.classList.remove("hidden");
  }
}

function disable(id) {
  const element = getElement(id);
  if (element != null) {
    element.disabled = true;
    element.classList.add("disabled");
  }
}

function enable(id) {
  const element = getElement(id);
  if (element != null) {
    element.disabled = false;
    element.classList.remove("disabled");
  }
}

function changeMode(newMode) {
  set("mode", newMode);
  set("modeChanged", "true");
  document.forms["piLoginForm"].submit();
}

function fallbackToOTP() {
  if (value("mode") !== "otp") {
    setTimeout(() => {
      changeMode("otp");
    }, 3000);
  }
}

function setMessage(newMessage) {
  getElement("message").innerText = newMessage;
}

function t(key) {
  return JSON.parse(getContent("privacyidea-translations"))[key];
}

function getWebAuthnErrorMessage(err) {
  switch (err.name) {
    case "AbortError":
    case "InvalidStateError":
    case "NotAllowedError":
    case "NotSupportedError":
    case "TypeError":
      return t("webauthn_" + err.name);
    default:
      return t("webauthn_other_error") + " (" + err.name + ")";
  }
}

function webAuthnError(err) {
  const errorMessage = getWebAuthnErrorMessage(err);
  console.log("Error while signing WebAuthnSignRequest: " + err);
  enable("useWebAuthnButton");
  setMessage(errorMessage);
  if (getElement("retryWebAuthnButton")) {
    show("retryWebAuthnButton");
  } else {
    const retryWebAuthnButton = getElement("useWebAuthnButton").cloneNode();
    retryWebAuthnButton.addEventListener("click", doWebAuthn);
    retryWebAuthnButton.id = "retryWebAuthnButton";
    retryWebAuthnButton.innerHTML = "<span>" + t("try_again") + "</span>";
    getElement("message").innerHTML += " ";
    getElement("message").appendChild(retryWebAuthnButton);
  }
}

function doWebAuthn() {
  disable("useWebAuthnButton");
  hide("retryWebAuthnButton");
  setMessage(t("webauthn_in_progress"));
  // If mode is push, we have to change it, otherwise the site will refresh while doing webauthn
  if (value("mode") === "push") {
    changeMode("webauthn");
  }

  if (!window.isSecureContext) {
    enable("useWebAuthnButton");
    setMessage(t("webauthn_insecure_context"));
    console.log(
      "Insecure context detected: Aborting Web Authn authentication!",
    );
    fallbackToOTP();
    return;
  }

  if (!window.pi_webauthn) {
    enable("useWebAuthnButton");
    setMessage(t("webauthn_library_unavailable"));
    fallbackToOTP();
    return;
  }

  const requestStr = value("webAuthnSignRequest");

  // Set origin
  if (!window.location.origin) {
    window.location.origin =
      window.location.protocol +
      "//" +
      window.location.hostname +
      (window.location.port ? ":" + window.location.port : "");
  }
  set("origin", window.origin);

  try {
    const requestjson = JSON.parse(requestStr);

    const webAuthnSignResponse = window.pi_webauthn.sign(requestjson);
    webAuthnSignResponse
      .then((webauthnresponse) => {
        const response = JSON.stringify(webauthnresponse);
        set("webAuthnSignResponse", response);
        set("mode", "webauthn");
        setMessage(t("webauthn_success"));
        document.forms["piLoginForm"].submit();
      })
      .catch(webAuthnError);
  } catch (err) {
    webAuthnError(err);
  }
}

function doU2F() {
  // If mode is push, we have to change it, otherwise the site will refresh while doing webauthn
  if (value("mode") === "push") {
    changeMode("u2f");
  }

  if (!window.isSecureContext) {
    setMessage(t("u2f_insecure_context"));
    console.log("Insecure context detected: Aborting U2F authentication!");
    fallbackToOTP();
    return;
  }

  const requestStr = value("u2fSignRequest");

  if (requestStr === null) {
    setMessage(t("u2f_unavailable"));
    fallbackToOTP();
    return;
  }

  try {
    const requestjson = JSON.parse(requestStr);
    sign_u2f_request(requestjson);
  } catch (err) {
    console.log("Error while signing U2FSignRequest: " + err);
    setMessage(t("u2f_sign_request_error") + " " + err);
  }
}

function sign_u2f_request(signRequest) {
  let appId = signRequest["appId"];
  let challenge = signRequest["challenge"];
  let registeredKeys = [];

  registeredKeys.push({
    version: "U2F_V2",
    keyHandle: signRequest["keyHandle"],
  });

  u2f.sign(appId, challenge, registeredKeys, function (result) {
    const stringResult = JSON.stringify(result);
    if (
      stringResult.includes("clientData") &&
      stringResult.includes("signatureData")
    ) {
      set("u2fSignResponse", stringResult);
      set("mode", "u2f");
      document.forms["piLoginForm"].submit();
    }
  });
}

function addClickListener(id, listener) {
  const el = getElement(id);
  if (el !== null) {
    el.addEventListener("click", listener);
  }
}

function initPrivacyIDEA() {
  // set preferred mode by JavaScript so that users without it always have "otp"
  const preferredMode = getElement("mode").dataset.preferred;
  if (preferredMode) {
    set("mode", preferredMode);
  }
  document.querySelectorAll(".js-show").forEach((el) => {
    el.classList.remove("hidden");
  });

  const step = getContent("privacyidea-step");

  if (step > 1) {
    hide("username");
    hide("password");
  } else {
    hide("otp");
    hide("message");
  }

  if (step > 1 && value("mode") !== "otp") {
    hide("otp");
    hide("submitButton");
  }

  if (value("mode") === "webauthn") {
    doWebAuthn();
  }

  if (value("mode") === "u2f") {
    doU2F();
  }

  if (value("mode") === "push") {
    const pollingIntervals = [4, 3, 2, 1];

    if (value("loadCounter") > pollingIntervals.length - 1) {
      refreshTime = pollingIntervals[pollingIntervals.length - 1];
    } else {
      refreshTime = pollingIntervals[Number(value("loadCounter") - 1)];
    }

    refreshTime *= 1000;
    setTimeout(() => {
      document.forms["piLoginForm"].submit();
    }, refreshTime);
  }

  addClickListener("useWebAuthnButton", doWebAuthn);
  addClickListener("usePushButton", () => changeMode("push"));
  addClickListener("useOTPButton", () => changeMode("otp"));
  addClickListener("useU2FButton", doU2F);
}

document.addEventListener("DOMContentLoaded", initPrivacyIDEA);
