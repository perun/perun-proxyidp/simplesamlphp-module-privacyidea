# [6.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/compare/v5.5.3...v6.0.0) (2024-04-25)


### chore

* remove rate limiting ([dd2299b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/commit/dd2299b33959f037502fd82c5058cfa4b3f85fef))


### BREAKING CHANGES

* dropped rate limiting support

## [5.5.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/compare/v5.5.2...v5.5.3) (2024-01-03)


### Bug Fixes

* enable backup code checking ([a4e16a1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/commit/a4e16a1095a99a39180d1c1b158c4a3662aafa05))
* failcounter handling ([61c3876](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/commit/61c3876fa5da17edb2222cbb980c38cb596c866d))

## [5.5.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/compare/v5.5.1...v5.5.2) (2023-08-31)


### Bug Fixes

* use flaps correctly ([a6b03e3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/commit/a6b03e36b61040cba6b14b0a2c40585c47d50889))

## [5.5.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/compare/v5.5.0...v5.5.1) (2023-07-18)


### Bug Fixes

* mfa skipping after logout ([419ce58](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/commit/419ce58781f613e45dd5dcc5ccf4f492dc9a4fa0))

# [5.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/compare/v5.4.2...v5.5.0) (2023-06-27)


### Features

* rate limiting ([c458740](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/commit/c458740d9cd18004b018326a433d7633ebff70a1))

## [5.4.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/compare/v5.4.1...v5.4.2) (2022-10-19)


### Bug Fixes

* improve wording of TOTP and WebAuthn ([ce96f25](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-privacyidea/commit/ce96f25c95b7860ade45aff8810663819a08f816))

## [5.4.1](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v5.4.0...v5.4.1) (2022-08-19)


### Bug Fixes

* bugfixes of twig template ([5f3bc24](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/5f3bc2402db9e2bebac92f9d742d5c619f5613c1))

# [5.4.0](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v5.3.2...v5.4.0) (2022-08-19)


### Features

* twig template, fix PSR-12 ([a318240](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/a3182408292030a7a8f34e17a240a3f2db1e3a21))

## [5.3.2](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v5.3.1...v5.3.2) (2022-07-28)


### Bug Fixes

* ensure that attributemap actually gets applied ([2761c1c](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/2761c1cfca95a69eeeded603d969bf3570d92aad))

## [5.3.1](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v5.3.0...v5.3.1) (2022-07-22)


### Bug Fixes

* save SSO data until end of session ([a022fe9](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/a022fe9c105b2d59869a03ee490b0303398e3930))

# [5.3.0](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v5.2.0...v5.3.0) (2022-06-09)


### Features

* turkish translation ([3dd33b4](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/3dd33b4e24987abe494915cbda607c9cbfd76f29))

# [5.2.0](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v5.1.0...v5.2.0) (2022-05-05)


### Features

* autofocus OTP field ([62cf7d6](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/62cf7d61876e5eb8758ec59e387a19614f0cdf97))

# [5.1.0](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v5.0.1...v5.1.0) (2022-05-05)


### Features

* timeout and connectTimeout options ([e5770e5](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/e5770e5c905cdce11ffe07c0998b1b5230c4d10a))

## [5.0.1](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v5.0.0...v5.0.1) (2022-03-30)


### Bug Fixes

* throw NoPassive when trying to do 2FA in passive request ([ae59aea](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/ae59aeabc9c7323a69819cb2cd26f9935eceb31f))

# [5.0.0](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v4.1.1...v5.0.0) (2022-03-25)


### Bug Fixes

* fix hiding of alternate methods ([4980b54](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/4980b54c0dc5fd8b2a26589dacfd583c4908e5cc))
* improve behavior when preferredToken=webAuthn in unsupported browser ([8ff3a55](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/8ff3a557ac5e9916451e814aa8907bb5f638078e))
* represent booleans with booleans ([980c73a](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/980c73a384068c3eab0f7974c01ef9daae4ab772))
* support OTP for users without JavaScript ([9f55899](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/9f5589996062f21fcc3eb75aa9144463be55e2e8))


### Features

* improve WebAuthn user experience ([f72b19b](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/f72b19b60e3ca452714ce00fe2f34efef50bc0da))
* messageOverride configuration option ([0e5db60](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/0e5db60ab49afb45ca2a38f8383243c41f39da0c))
* translation improvements ([cb7f384](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/cb7f384a723f9c057c54cc46b8cafb1ff9ca1e60))


### BREAKING CHANGES

* renamed dictionary entries, changed HTML structure
* configuration options need to be booleans instead of strings "true" and "false"

## [4.1.1](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v4.1.0...v4.1.1) (2022-03-24)


### Bug Fixes

* improve error messages ([662b090](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/662b09064aa0e2fcd5dda9495d0d1d98086da655))
* typo ([272b18a](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/272b18a1dcf59bf8d30adda0628c986eea573cd3))

# [4.1.0](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v4.0.1...v4.1.0) (2022-03-23)


### Bug Fixes

* bring back HTML fixes which were silently reverted in a merge commit ([19416f3](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/19416f384e2d957bd80a01d7fe6748d2f460bab7))


### Features

* showLogout configuration option ([2b8636b](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/2b8636b3ec27188fffc7bb5bbd072f3812c9a343))

## [4.0.1](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v4.0.0...v4.0.1) (2022-03-23)


### Bug Fixes

* revert weird CSS introduced with SSO ([a99c830](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/a99c83078bc453fb1b762d2cde596ba988be69e3))

# [4.0.0](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v3.0.2...v4.0.0) (2022-03-22)


### Bug Fixes

* fix disabling filter by previous filter ([34a06a8](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/34a06a839fb63fbb28ee05dffe042fe58225c84a))


### BREAKING CHANGES

* method isPrivacyIDEADisabled moved from Utils to PrivacyideaAuthProc

## [3.0.2](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v3.0.1...v3.0.2) (2022-03-22)


### Bug Fixes

* prevent warning when printing username ([8f82809](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/8f828096fc789d4d5ddbbff1a367de382c0627ae))

## [3.0.1](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v3.0.0...v3.0.1) (2022-03-22)


### Bug Fixes

* coalesce instead of suppressing errors ([562a72f](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/562a72f4db2a8c631fb9c7b4cd27a97e7f77e622))
* convert int to str before printing ([6a6978e](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/6a6978eef4f37805cdee5811eb06350b7af9d948))
* correct test if username is not empty ([7f4ff7b](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/7f4ff7bd342b898875b6fd1fdb68a0a385ea47e4))
* only print username if it is nonempty ([1ddeda0](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/1ddeda04dd6b19c013069b34f262e90857fa62b7))
* set default username to empty string ([3041a3e](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/3041a3eab9fc0a17250baa35633b26ddfa19ed51))
* unify boolean representation ([1f461c6](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/1f461c622ec4874369707b49d145510a723e9a5a))

# [3.0.0](https://github.com/CESNET/simplesamlphp-module-privacyidea/compare/v2.1.0...v3.0.0) (2022-03-22)


### Bug Fixes

* correct otp message translation ([0f90324](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/0f903244027716fa787801c9b1fd97bd52520099))
* correct usage of redirectTrustedURL, stop using deprecated Utilities ([0c3d195](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/0c3d1958947a7472881224c8062d4da9d20a28d7))


### Features

* alter forked repository ([b4113a5](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/b4113a563713ee3d7051cf1fcad22b830fee1678))
* move from SimpleSAML_ classes to namespaces ([e23cc3c](https://github.com/CESNET/simplesamlphp-module-privacyidea/commit/e23cc3c19b1fbf45342c77cab3bb779407920978))


### BREAKING CHANGES

* minimum SSP version is 1.17.0
* new package name, use forked PHP client, remove debian/ubuntu packaging, remove google theme
